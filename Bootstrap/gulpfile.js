/// <binding BeforeBuild='default' />
var gulp = require('gulp');
var gutil = require('gulp-util');
var inject = require('gulp-inject');
var wiredep = require('wiredep');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cleanCss = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');

var webroot = "./src/";
var paths = {
    ngComponents: './src/app/components/**/*.js',
    ngServices: './src/app/services/**/*.service.js',
    ngDirectives: './src/app/directives/**/*.js',
    ngConfigs: './src/app/config/**/*.js',
    ngConstants: './src/app/constants/**/*.js',
    styles: './src/css/**/*.css',
    stylesMin: '!./src/css/**/*.min.css',
    bundleJs: './src/bundle/*.min.js',
    bundleCss: './src/bundle/*.min.css'
};

gulp.task('default', function () {
    // Inject scripts, css
    var wiredepPaths = wiredep();
    var scripts = [
        paths.ngComponents,
        paths.ngServices,
        paths.ngDirectives,
        paths.ngConfigs,
        paths.ngConstants,
    ];
    var styles = [
        paths.styles,
        paths.stylesMin
    ];
    scripts = wiredepPaths.js.concat(scripts);
    styles = wiredepPaths.css.concat(styles);

    var scriptSrc = gulp.src(scripts, { read: false });
    var styleSrc = gulp.src(styles, { read: false });
    gulp.src(webroot + 'app/components/index.html')
        .pipe(inject(scriptSrc, {name: 'inject'}))
        .pipe(inject(styleSrc, { name: 'inject' }))
        .pipe(gulp.dest(webroot + 'app/components'));
});

gulp.task('default-min', function () {
    // Inject scripts, css
    var wiredepPaths = wiredep();
    var scripts = [
        paths.ngComponents,
        paths.ngServices,
        paths.ngDirectives,
        paths.ngConfigs,
        paths.ngConstants
    ];
    var styles = [
        paths.styles,
        paths.stylesMin
    ];
    scripts = wiredepPaths.js.concat(scripts);
    styles = wiredepPaths.css.concat(styles);

    // Generate gulp source
    var scriptSrc = gulp.src(scripts);
    var styleSrc = gulp.src(styles);

    // Minify and bundle with source mapping
    scriptSrc
        .pipe(uglify())
        .pipe(concat('bundle.min.js'))
        .pipe(gulp.dest(webroot + 'bundle'));

    styleSrc
        .pipe(sourcemaps.init())
        .pipe(cleanCss())
        .pipe(concat('bundle.min.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(webroot + 'bundle'));

    var bundleJsSrc = gulp.src([paths.bundleJs], {read: false});
    var bundleCssSrc = gulp.src([paths.bundleCss], { read: false });

    // Inject to default page
    gulp.src(webroot + 'app/components/index.html')
        .pipe(inject(bundleJsSrc, {name: 'inject'}))
        .pipe(inject(bundleCssSrc, { name: 'inject' }))
        .pipe(gulp.dest(webroot + 'app/components'));
});