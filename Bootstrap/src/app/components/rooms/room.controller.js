﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('RoomController', RoomController);

    RoomController.$inject = ['$scope', '$http', '$state', '$stateParams', 'tribee'];

    function RoomController($scope, $http, $state, $stateParams, tribee) {
        var vm = this;

        init();

        function init() {
            $scope.slides = [];
            $scope.checkInDate = '';
            $scope.checkOutDate = '';
            $scope.hostel = "";
            $scope.hostelList = tribee.hostelList;
        }
    }
})();