﻿(function () {
    'use strict';

    angular
        .module('app')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider'];

    function routeConfig($stateProvider) {
        $stateProvider
            .state('hostel.room',
            {
                url: "/hostels/{hostel}",
                views: {
                    '': {
                        templateUrl: "/src/app/components/rooms/room.html",
                        controller: "RoomController",
                        controllerAs: "vm"
                    }
                }
            });
    }
})();