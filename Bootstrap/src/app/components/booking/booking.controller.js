﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('BookingController', BookingController);

    BookingController.$inject = ['$scope', '$http', '$state', '$stateParams', 'tribee'];

    function BookingController($scope, $http, $state, $stateParams, tribee) {
        var vm = this;

        init();

        function init() {
            $scope.slides = [];
            $scope.checkInDate = new Date($stateParams.checkIn);
            $scope.checkOutDate = new Date($stateParams.checkOut);
            if (isNaN($scope.checkInDate.getTime())) {
                $scope.checkInDate = '';
            }
            if (isNaN($scope.checkOutDate.getTime())) {
                $scope.checkOutDate = '';
            }
            $scope.hostel = $stateParams.hostel;
            $scope.hostelList = tribee.hostelList;
        }
    }
})();