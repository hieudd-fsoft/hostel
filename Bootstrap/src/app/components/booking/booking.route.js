﻿(function () {
    'use strict';

    angular
        .module('app')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider'];

    function routeConfig($stateProvider) {
        $stateProvider
            .state('booking',
            {
                url: "/booking?checkIn&checkOut&hostel",
                views: {
                    '': {
                        templateUrl: "/src/app/components/booking/booking.html",
                        controller: "BookingController",
                        controllerAs: "vm"
                    }
                }
            });
    }
})();