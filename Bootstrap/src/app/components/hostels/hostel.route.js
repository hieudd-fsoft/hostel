﻿(function () {
    'use strict';

    angular
        .module('app')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider'];

    function routeConfig($stateProvider) {
        $stateProvider
            .state('hostel',
            {
                url: "/hostels/{hostel}",
                views: {
                    '': {
                        templateUrl: "/src/app/components/hostels/hostel.html",
                        controller: "HostelController",
                        controllerAs: "vm"
                    }
                }
            });
    }
})();