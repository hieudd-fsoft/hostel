﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('HostelController', HostelController);

    HostelController.$inject = ['$scope', '$http', '$state', '$stateParams', 'tribee'];

    function HostelController($scope, $http, $state, $stateParams, tribee) {
        var vm = this;

        init();

        function init() {
            $scope.slides = [];
            $scope.checkInDate = '';
            $scope.checkOutDate = '';
            $scope.hostel = "";
            $scope.hostelList = tribee.hostelList;
        }
    }
})();