﻿(function () {
    'use strict';

    angular
        .module('app')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider'];

    function routeConfig($stateProvider) {
        $stateProvider
            .state('home',
            {
                url: "/",
                views: {
                    '': {
                        templateUrl: "/src/app/components/home/home.html",
                        controller: "HomeController",
                        controllerAs: "vm"
                    }
                }
            });
    }
})();