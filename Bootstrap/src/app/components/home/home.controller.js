﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', '$http', '$state', '$stateParams', 'tribee'];

    function HomeController($scope, $http, $state, $stateParams, tribee) {
        var vm = this;
        vm.submit = submit;

        init();

        function init() {
            //$scope.slides = [];
            //$scope.checkInDate = '';
            //$scope.checkOutDate = '';
            //$scope.hostel = "";
            $scope.hostelList = tribee.hostelList;
        }

        function submit() {
            console.log($scope.bookingForm);
            if (!$scope.bookingForm.$invalid) {
                $state.go('booking',
                {
                    checkIn: $scope.checkInDate,
                    checkOut: $scope.checkOutDate,
                    hostel: $scope.hostel
                });
            }
        }
    }
})();