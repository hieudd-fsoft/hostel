﻿angular.module('table.sorting', ['template/tableSorting.html'])

.controller('TableSortingController', ['$scope', '$attrs', '$parse', function ($scope, $attrs, $parse) {
    var ctrl = this;
}])

.directive('tableSorting', ['$parse', function ($parse) {
    return {
        require: ['tableSorting'],
        restrict: 'A',
        controller: 'TableSortingController',
        controllerAs: 'tableSorting',
        templateUrl: function (element, attrs) {
            return attrs.templateUrl || 'template/tableSorting.html';
        },
        replace: true
    };
}]);

angular.module("template/tableSorting.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/tableSorting.html",
  '<span class="glyphicon glyphicon-sort pull-right"></span>');
}]);