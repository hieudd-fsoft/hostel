﻿(function () {
    'user strict';

    angular
        .module('app')
        .directive('popoverEditable', function () {
            return {
                restrict: 'A',
                link: function(scope, element) {
                    $(element)
                        .popover({
                                content: '<input class="form-control" /> ' +
                                    '<input class="form-control" />' +
                                    '<button class="btn btn-primary">Update</button>'
                        });
                }
            };
        });
})();