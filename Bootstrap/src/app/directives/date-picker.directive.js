﻿(function () {
    'use strict';

    angular
        .module('bootstrap.datepicker', [])
        .controller('DatePickerController',
        [
            '$scope', '$attrs', function ($scope, $attrs) {
                var ctrl = this;
                ctrl.init = init;

                function init(ngModelCtrl, element) {
                    ctrl.ngModelCtrl = ngModelCtrl;
                    var today = new Date();
                    $(element)
                        .datepicker({
                            autoclose: true,
                            todayHighlight: true,
                            startDate: today
                        })
                        .on('changeDate',
                            function (e) {
                                ctrl.ngModelCtrl.$setViewValue(e.date);
                            });

                    $scope.$watch(ngModelCtrl, function () {
                        $(element)
                            .datepicker('setDate', ctrl.ngModelCtrl.$viewValue);
                    });
                }
            }
        ])
        .directive('datePicker',
        [
            function () {
                return {
                    scope: {
                    },
                    require: ['datePicker', '?ngModel'],
                    restrict: 'A',
                    controller: 'DatePickerController',
                    controllerAs: 'datePicker',
                    link: function (scope, element, attrs, ctrls) {
                        var datePickerCtrl = ctrls[0];
                        var ngModelCtrl = ctrls[1];

                        if (!ngModelCtrl) {
                            return; // do nothing if no ng-model
                        }

                        datePickerCtrl.init(ngModelCtrl, element);
                    }
                };
            }
        ]);
})();