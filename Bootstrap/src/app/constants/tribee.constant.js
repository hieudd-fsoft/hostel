﻿(function () {
    'user strict';

    angular
        .module('app')
        .constant('tribee',
        {
            hostelList: [
                {
                    name: "Tribee Cham, Hoi An",
                    value: 1
                },
                {
                    name: "Tribee Kinh, Hoi An",
                    value: 2
                }
            ]
        });
})();